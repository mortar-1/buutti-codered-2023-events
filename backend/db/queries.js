const createTables = `
CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    username varchar(20) NOT NULL UNIQUE,
    password varchar(20) NOT NULL,
    PRIMARY KEY(id)
  );
  
  CREATE TABLE IF NOT EXISTS favorites (
    id SERIAL,
    event_id varchar NOT NULL,
    name varchar NOT NULL,
    end_date varchar,
    user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    PRIMARY KEY(id)
  );`;

export default { createTables };