import pg from "pg";

import * as queries from "./queries.js";

const isProd = process.env.NODE_ENV === "production";

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env;

export const pool = new pg.Pool({
  host: PG_HOST,
  port: PG_PORT,
  user: PG_USERNAME,
  password: PG_PASSWORD,
  database: PG_DATABASE,
  ssl: isProd
});

export const executeQuery = async(query, parameters) => {
  const client = await pool.connect();
  try {
    const result = await client.query(query, parameters);
    return result;
  } catch (error) {
    console.error(error.stack);
    error.name = "Database error";
    throw error;
  } finally {
    client.release();
  }
};

export const createTables = async () => {
  await executeQuery(queries.default.createTables);
  console.log("Tables created in database if not exist");
};

export const registerUser = async (query,parameters) => {
  return await executeQuery(query, parameters);
};

export const loginUser = async (query, parameters) => {
  const result = await executeQuery(query, parameters);
  return result;
};

export const addFavorites = async (query,parameters) => {
  console.log("addfavorites called");
  return await executeQuery(query,parameters);
};

export const getFavorites = async (query,parameters) => {
  const result = await executeQuery(query,parameters);
  return result;
};

export const removeFavorite = async (query,parameters) => {
  return await executeQuery(query,parameters);
};


export default executeQuery;