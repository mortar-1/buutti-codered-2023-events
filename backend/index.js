import express from "express"
import path from "path"
import url from "url"
import appdata from "./package.json" assert { type: 'json' };
import fetch from "node-fetch";
import cors from "cors";
import { createTables, registerUser, loginUser, addFavorites, getFavorites, removeFavorite } from './db/db.js'

const app = express()
app.use(express.json())
app.use(cors())

await createTables()

app.get('/api/version', (req, res) => {
    let version = appdata.version;
    res.send(version);
})

app.get('/api/tags', async (req, res) => {
    let tagsData;
    let tags = [];
    try {
        await fetch('http://open-api.myhelsinki.fi/v1/events/')
            .then(res => res.json())
            .then(json => json.tags)
            .then(data => tagsData = data)
        for (const key in tagsData) {
            tags.push(tagsData[key]);
        }
        res.status(200);
        res.json(tags);
    } catch (error) {
        res.status(400);
        res.json(error);
    }
})

app.get('/api/event/:eventid', async (req, res) => {
    let event;
    let url = 'http://open-api.myhelsinki.fi/v1/event/' + req.params.eventid;
    try {
        await fetch(url)
            .then(res => res.json())
            .then(json => {
                event = json;
            });
        res.status(200);
        res.json(event);
    } catch (error) {
        res.status(400);
        res.json(error);
    }
})

app.get('/api/events', async (req, res) => {
    let events;
    try {
        await fetch('http://open-api.myhelsinki.fi/v1/events/')
            .then(res => res.json())
            .then(json => json.data)
            .then(data => {
                events = data;
            })
        res.status(200);
        res.json(events);
    } catch (error) {
        res.status(400);
        res.json(error);
    }
})

app.get('/api/events/:limit/:start', async (req, res) => {
    let events;
    let start = req.params.start;
    let limit = req.params.limit;
    try {
        await fetch('https://open-api.myhelsinki.fi/v1/events/?limit=' + limit + '&start=' + start)
            .then(res => res.json())
            .then(json => json.data)
            .then(data => {
                events = data;
            })
        res.status(200);
        res.json(events);
    } catch (error) {
        res.status(400);
        res.json(error);
    }
});

app.get('/api/events/:limit/:start/:tags', async (req, res) => {
    let events;
    let start = req.params.start;
    let limit = req.params.limit;
    let tags = req.params.tags;
    try {
        await fetch(
            'https://open-api.myhelsinki.fi/v1/events/?limit=' + limit + '&start=' + start + '&tags_filter=' + encodeURI(tags)
        )
            .then(res => res.json())
            .then(json => {
                events = json.data
            }
            );
        res.status(200);
        res.json(events);
    } catch (error) {
        console.log(error);
        console.log("Error");
        res.status(400);
        res.json(error);
    }
});

app.post('/api/register', async (req, res) => {
    const username = req.body.username
    const password = req.body.password

    if (!username || !password) {
        return res.status(400).send({ message: "username and password required" });
    }
    try {
        const queryString = `
          INSERT INTO users (username, password) VALUES ($1,$2)
          RETURNING id, username
        ;`

        const savedUser = await registerUser(queryString, [username, password]);
        res.status(200).send(savedUser.rows[0]);
    } catch (e) {
        console.log(e);
        res.status(400).send({ message: e });
    }
})

// api for login here???
app.post('/api/login', async (req, res) => {
    const user = req.body.user
    const password = req.body.password

    if (!user || !password) {
        return res.status(400).send({ message: "username and password required" });
    }
    try {
        const queryString = `
            SELECT id, username FROM users WHERE username=$1 and password=$2
        ;`
        const result = await loginUser(queryString, [user, password])
        if (!result.rows || !result.rows[0]) {
            return res.status(400).send({ message: "invalid username or password" });
        }
        // What do we return on succesfull login?
        res.status(200).send(result.rows[0]);
    } catch (e) {
        console.log(e);
        res.status(400).send({ message: e });
    }
})

app.post('/api/favorite', async (req, res) => {
    const event_id = req.body.eventid
    const name = req.body.eventname
    const end_date = req.body.enddate
    const user_id = req.body.user
    //const{event_id, name, end_date, user_id} = req.body
    console.log('Body: ', req.body)
    try {
        const query = `
            INSERT INTO favorites (event_id, name, end_date, user_id) VALUES ($1,$2,$3,$4)
            RETURNING id, event_id, name, end_date
        ;`

        console.log('values:', event_id, name, end_date, user_id)
        console.log('query: ', query)

        const added = await addFavorites(query, [event_id, name, end_date, user_id]);
        res.status(200).send(added.rows[0]);
    } catch (e) {
        console.log(e);
        res.status(400).send({ message: e });
    }
});

app.get('/api/favorites/:userId', async (req, res) => {
    try {
        const user = req.params.userId;
        console.log(user);
        const query = `SELECT id, event_id,name,end_date FROM favorites WHERE user_id=$1`
        const list = await getFavorites(query, [user])
        res.status(200).send(list.rows)
    } catch (e) {
        console.log(e)
        res.status(400).send(e)
    }
})

app.delete('/api/:userId/favorite/:eventId', async (req, res) => {
    try {
        const user = req.params.userId
        const eventId = req.params.eventId
        const query = `DELETE FROM favorites WHERE user_id=$1 and event_id=$2;`
        await removeFavorite(query, [user, eventId])
        console.log(user, eventId)
        res.status(200).send()
    } catch (e) {
        console.log(e)
        res.status(400).send(e)
    }
})

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url));
const distDirectory = path.resolve(currentDirectory, "./dist/");
app.use("/", express.static(distDirectory));
app.get("*", (_req, res) => {
    res.sendFile("index.html", { root: distDirectory });
});

// Serve
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})