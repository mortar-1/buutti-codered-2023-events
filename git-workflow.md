# GIT workflow for project

## Before you start to code
- git checkout main
- git pull
- git branch featureName
- git checkout featureName

## Code
- code your feature

## After you are finished
- git add .
- git commit -m"Commit message"
- git push origin featureName

## After code is in gitlab
- create merge request
- ask someone to review it

## Finally, you can delete the local branch
- git checkout main
- git branch -d featureName