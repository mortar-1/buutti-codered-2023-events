import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";

import AppContextProvider from "./Context/AppContext.jsx";
import ErrorPage from "./Pages/Error";
import MapPage from "./Pages/MapPage";
import Event from "./Pages/Event";
import Events from "./Pages/Events";
import Favourites from "./Pages/Favourites.jsx";
import Layout from "./Pages/Layout";
import LoginPage from "./Pages/Login";
import RegisterPage from "./Pages/Register";
import utils from "./Utilities/apiFetch.js";
import "./main.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/events",
        element: <Events />,
      },
      {
        path: "/map",
        element: <MapPage />,
      },
      {
        path: "/events/:event",
        element: <Event />,
        loader: utils.eventLoader,
      },
      {
        path: "/login",
        element: <LoginPage />
      },
      {
        path: "/register",
        element: <RegisterPage />
      },
      {
        path: "/user/favourites",
        element: <Favourites />
      }
    ]
  }
]);

// eslint-disable-next-line no-undef
ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <AppContextProvider>
      <RouterProvider router={router} />
    </AppContextProvider>
  </React.StrictMode>,
);
