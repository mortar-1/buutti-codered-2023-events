import axios from "axios";
import { json } from "react-router-dom";

const eventLoader = async ({ params }) => {
  try {
    const eventResponse = await axios.get(`/api/event/${params.event}`);
    return eventResponse.data;
  } catch (error) {
    console.log(error);
    throw json({}, { status: 404 });
  }
};

const fetchAllEvents = async (start = 0, limit = 30) => {
  try {
    const eventsResponse = await axios.get(`/api/events/${limit}/${start}`);
    return eventsResponse.data;
  } catch (error) {
    console.log(error);
    throw json({}, { status: 404 });
  }
};

const fetchByTags = async (tags, start = 0) => {
  let parsedTags = "";
  tags.forEach(tag => {
    parsedTags += tag + ",";
  });
  const eventsByTags = await axios.get(`/api/events/30/${start}/${parsedTags}`);
  return eventsByTags.data;
};

const fetchAllTags = async () => {
  const allTags = await axios.get("/api/tags");
  return allTags.data;
};

const fetchFavourites = async user => {
  const favs = await axios.get(`/api/favorites/${user.id}`);
  return favs.data;
};

const postLogin = async (user, password) => {
  const loggedUser = await axios.post("/api/login", {
    user, password
  });
  return loggedUser.data;
};

const postRegister = async (username, password) => {
  const newUser = await axios.post("/api/register", {
    username, password
  });
  return newUser.data;
};

const postFavorite = async (eventid, eventname, enddate, user) => {
  const newFavorite = await axios.post("/api/favorite", {
    eventid, eventname, enddate, user
  });
  return newFavorite.data;
};

const deleteFavorite = async (user, eventid) => {
  await axios.delete(`/api/${user}/favorite/${eventid}`)
};

export default {
  eventLoader,
  fetchAllEvents,
  fetchByTags,
  fetchAllTags,
  fetchFavourites,
  postLogin,
  postRegister,
  postFavorite,
  deleteFavorite
};
