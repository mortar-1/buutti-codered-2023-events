import { useState, useEffect, createContext } from "react";
import api from "../Utilities/apiFetch";

export const AppContext = createContext(null);

const AppContextProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [tags, setTags] = useState(null);

  useEffect(() => {
    fetchAllTags();
  }, []);

  const fetchAllTags = async () => {
    try {
      const allTags = await api.fetchAllTags();
      setTags(allTags);
    } catch(e) {
      console.log(e);
    }
  };

  const loginUser = user => {
    setUser(user);
  };

  const logoutUser = () => {
    setUser(null);
  };

  return (
    <AppContext.Provider value={{
      user,
      loginUser,
      logoutUser,
      tags
    }}>
      {children}
    </AppContext.Provider>
  );
};

export default AppContextProvider;