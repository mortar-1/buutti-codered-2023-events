import { useState, useEffect } from "react";

import EventList from "../Components/EventList";
import Loader from "../Components/Loader";
import TagSearch from "../Components/TagSearch";
import api from "../Utilities/apiFetch";


const Events = () => {
  const [events, setEvents] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedTags, setSelectedTags] = useState([]);

  useEffect(() => {
    fetchEvents(0);
  }, []);

  const fetchEvents = async (start) => {
    setIsLoading(true);
    const eventsList = await api.fetchAllEvents(start);
    setEvents(eventsList);
    setIsLoading(false);
  };

  const fetchMoreEvents = async () => {
    setIsLoading(true);
    const extendedEventList = selectedTags.length
      ? await api.fetchByTags(selectedTags, events.length)
      : await api.fetchAllEvents(events.length);
    setEvents(events.concat(extendedEventList));
    setIsLoading(false);
  };

  const fetchWithTags = async () => {
    if (!selectedTags.length) return;
    setIsLoading(true);
    const byTags = await api.fetchByTags(selectedTags);
    setEvents(byTags);
    setIsLoading(false);
  };

  const addToSelectedTags = tag => {
    setSelectedTags(selectedTags.concat(tag));
  };

  const clearSelectedTags = async () => {
    setSelectedTags([]);
    setEvents([]);
    await fetchEvents(0);
  };

  return (
    <div className="Events">
      <h1>Events</h1>
      <TagSearch
        apiFetch={fetchWithTags}
        addTag={addToSelectedTags}
        selectedTags={selectedTags}
        clearTags={clearSelectedTags}
      />
      <EventList events={events} />
      <Loader isLoading={isLoading} />
      <button onClick={() => fetchMoreEvents()}>Show more</button>
    </div>
  );
};

export default Events;
