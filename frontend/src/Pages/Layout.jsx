import { Outlet } from "react-router-dom";

import Navigation from "../Components/Navigation";
import "./Layout.css";

const Layout = () => {
  return (
    <div className="layout-wrapper">
      <Navigation />
      <main className="main">
        <h1 className="main-h1">Heleventti</h1>
        <Outlet />
      </main>
    </div>
  );
};

export default Layout;