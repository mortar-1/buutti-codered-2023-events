import { useLoaderData } from "react-router-dom";
import { useContext, useState, useEffect } from "react";
import { EventName, EventDate, EventAddress, EventDescription } from "../Components/EventListEvent";

import { AppContext } from "../Context/AppContext";
import api from "../Utilities/apiFetch.js";


const Event = () => {
  const event = useLoaderData();
  const { user } = useContext(AppContext);
  const [isFavorite, setIsFavorite] = useState(false);

  useEffect(() => {
    checkIfFavorite();
  }, []);

  const checkIfFavorite = async () => {
    if (user) {
      const favList = await api.fetchFavourites(user);
      const found = favList.find(f => f.event_id === event.id);
      if (found) setIsFavorite(true);
    }
  };

  const handleAddFavorite = async () => {
    try {
      const eventid = event.id;
      const eventnamme = event.name["fi"];
      const enddate = event.event_dates.ending_day;
      await api.postFavorite(eventid, eventnamme, enddate, user.id);
      setIsFavorite(true);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteFavorite = async () => {
    try {
      await api.deleteFavorite(user.id, event.id);
      setIsFavorite(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="Event">
      <EventName name={event.name} lang="fi" styling="event-list-name" />
      {(!isFavorite && user) && <button onClick={handleAddFavorite}>Lisää suosikkeihin</button>}
      {(isFavorite && user) && <button onClick={handleDeleteFavorite}>Poista suosikeista</button>}
      <div className="event-list-dates event-list-small-text">
        <EventDate date={event.event_dates.starting_day} />
        <p> - </p>
        <EventDate date={event.event_dates.ending_day} />
      </div>
      <EventAddress
        address={event.location.address}
        postalCode={true}
        city={true}
        styling="event-list-dates event-list-small-text"
      />
      <EventDescription description={event.description} />
    </div>
  );
};

export default Event;