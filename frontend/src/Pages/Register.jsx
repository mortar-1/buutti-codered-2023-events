import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";

import { AppContext } from "../Context/AppContext.jsx";
import api from "../Utilities/apiFetch.js";

const LoginPage = () => {
  const navigate = useNavigate();
  const { loginUser } = useContext(AppContext);
  const [username, setUsername] = useState("");
  const [checkUsername, setCheckUsername] = useState("");
  const [password, setPassword] = useState("");
  const [checkPass, setCheckPass] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async event => {
    try {
      event.preventDefault();
      if (username !== checkUsername || password !== checkPass) {
        setError("Please check your usernames and passwords match");
        return;
      }
      const newUser = await api.postRegister(username, password);
      console.log(newUser);
      loginUser(newUser);
      navigate("/user/favourites");
    } catch(e) {
      console.log(e);
      const message = e.response.data.message || e.message;
      setError(message);
    }
  };

  const isDisabled = !username && !password;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>username:</label>
        <input
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
      </div>
      <div>
        <label>confirm username:</label>
        <input
          value={checkUsername}
          onChange={e => setCheckUsername(e.target.value)}
        />
      </div>
      <div>
        <label>password:</label>
        <input
          value={password}
          type="password"
          onChange={e => setPassword(e.target.value)}
        />
      </div>
      <div>
        <label>confirm password:</label>
        <input
          value={checkPass}
          type="password"
          onChange={e => setCheckPass(e.target.value)}
        />
      </div>
      <p>{error}</p>
      <button
        type="submit"
        disabled={isDisabled}
      >Login</button>
    </form>
  );
};

export default LoginPage;