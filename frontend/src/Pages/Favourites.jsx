import { useState, useEffect, useContext } from "react";

import { AppContext } from "../Context/AppContext";
import { FavouriteListEvent } from "../Components/EventListEvent";
import Loader from "../Components/Loader";
import api from "../Utilities/apiFetch";

const Favourites = () => {
  const { user } = useContext(AppContext);
  const [favourites, setFavourites] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getFavourites();
  }, [user]);

  const getFavourites = async () => {
    if (!user) return;
    try {
      const favs = await api.fetchFavourites(user);
      setFavourites(favs);
      setIsLoading(false);
    } catch (e) {
      console.log(e);
    }
  };

  if (!user) return null;

  const LoaderOrList = isLoading
    ? <Loader isLoading={isLoading} />
    : (
      <div>
        <ul className="event-list">
          {favourites.map(e =>
            <FavouriteListEvent
              key={e.id}
              event={e}
            />
          )}
        </ul>
      </div>
    );

  return (
    <>
      <h2>{user.username} - Favourites</h2>
      {LoaderOrList}
    </>
  );
};

export default Favourites;