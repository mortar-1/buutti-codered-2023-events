import { useEffect, useRef, useState } from "react";
import { useMapEvents, Tooltip, Marker, Popup, MapContainer, TileLayer, useMap } from "react-leaflet";
import utils from "../Utilities/apiFetch";

const MapPage = () => {
  const [events, setEvents] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [search, setSearch] = useState("");
  const [searchText, setSearchText] = useState("");
  const [center, setCenter] = useState([60.1707567, 24.9401092]);
  const [position, setPosition] = useState(null);
  const searchInput = useRef();

  useEffect(() => {
    fetchEvents();
  }, []);

  const fetchEvents = async () => {
    if(isLoading) {
      console.log("Is Loading");
      setIsLoading(false);
      let eventsList = await utils.fetchAllEvents(0,1000);
      setEvents(eventsList);
    }
  };

  const searchButton = async () => {
    console.log(searchInput.current.value);
    setSearch(searchInput.current.value);
    searchInput.current.value = "";
    //setSearchText("");
  };

  const LocationSearch = () => {
    const map = useMapEvents({
      locationfound(e) {
        console.log("Location found");
        map.flyTo(e.latlng, 13);
      },
    });
    const searchLocation = () => {
      map.locate();
    };
    return(
      <div className="locationForeground">
        <button onClick={searchLocation}>Hae sijaintisi</button>
      </div>
    );
  };

  const EventMarkers = () => {
    let eventsArray = [];
    for(let i = 0; i < events.length; i++) {
      let mapEvent = events[i];
      let id = mapEvent.id;
      let name = mapEvent.name.fi;
      let description;
      if(mapEvent.description.intro !== undefined && mapEvent.description.intro !== null) {
        description = mapEvent.description.intro;
      } else {
        description = "Kuvaus puuttuu";
      }
      let lat = mapEvent.location.lat;
      let lon = mapEvent.location.lon;
      let inSearch = true;
      let lowerCaseDescription = description.toLowerCase();
      if(search === "") {
        inSearch = true;
      } else {
        if(lowerCaseDescription.includes(search)) {
          inSearch = true;
        } else {
          inSearch = false;
        }
      }
      if(inSearch) {
        let eventJSX =
          <Marker id={i} key={id} position={[lat, lon]} eventHandlers={{
            click: (event) => {
              eventMarkerClick(event);
            },
          }}>
            <Tooltip>
              {name}
            </Tooltip>
            <Popup>
              <h3>{name}</h3>
              {description}
            </Popup>
          </Marker>;
        eventsArray.push(eventJSX);
      }
    }
    return(eventsArray);
  };

  const eventMarkerClick = (event) => {
    console.log(event.target.options.id);
  };

  return (
    <>
      <MapContainer center={center} zoom={13} scrollWheelZoom={false}>
        <div className="searchForeground">
          <input type="text" id="searchText" ref={searchInput} ></input>
          <button onClick={searchButton}>Tapahtumahaku</button>
        </div>
        <LocationSearch></LocationSearch>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <EventMarkers></EventMarkers>
      </MapContainer>
    </>
  );
};
export default MapPage;