import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";

import { AppContext } from "../Context/AppContext.jsx";
import api from "../Utilities/apiFetch.js";

const LoginPage = () => {
  const { loginUser } = useContext(AppContext);
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const handleSubmit = async event => {
    try {
      event.preventDefault();
      const loggedUser = await api.postLogin(username, password);
      loginUser(loggedUser);
      navigate("/user/favourites");
    } catch(e) {
      console.log(e);
      const message = e.response.data.message || e.message;
      setError(message);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>username:</label>
        <input
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
      </div>
      <div>
        <label>password:</label>
        <input
          value={password}
          type="password"
          onChange={e => setPassword(e.target.value)}
        />
      </div>
      <p>{error}</p>
      <button
        type="submit"
        disabled={!username && !password}
      >Login</button>
    </form>
  );
};

export default LoginPage;