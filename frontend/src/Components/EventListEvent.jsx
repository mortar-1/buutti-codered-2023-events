import { Link } from "react-router-dom";

export const EventName = ({ name, lang, styling = "" }) => {
  if (!name[lang]) return null;
  return (
    <p className={styling}>{name[lang]}</p>
  );
};

export const EventDate = ({ date, styling = "" }) => {
  if (!date) return null;
  const parsedDate = new Date(date).toLocaleDateString("fi", {
    year: "numeric", month: "numeric", day: "numeric"
  });

  return (
    <p className={styling}>{parsedDate}</p>
  );
};

export const EventAddress = ({ address, postalCode = false, city = false, styling = "" }) => {
  if (!address || !address.street_address) return null;
  return (
    <span className={styling}>
      <p>{address.street_address + ", "}</p>
      {postalCode && <p>{address.postal_code + ", "}</p>}
      {city && <p>{address.locality}</p>}
    </span>
  );
};

export const EventIntro = ({ intro, styling = "" }) => {
  if (!intro) return null;
  return (
    <p className={styling}>{intro}</p>
  );
};

export const EventDescription = ({ description, styling = "" }) => {
  const body = description.body;
  const imgUrl = description.images[0].url;
  return (
    <div className={styling}>
      {body && <p dangerouslySetInnerHTML={{ __html: body }}></p>}
      {imgUrl && <img src={imgUrl}></img>}
    </div>
  );
};

export const FavouriteListEvent = ({ event }) => {
  const name = { fi: event.name };
  return (
    <li className="event-list event-list-item">
      <Link to={`/events/${event.event_id}`} className="event-list-link">
        <EventName name={name} lang="fi" styling="event-list-name" />
        <EventDate date={event.end_date} />
      </Link>
    </li>
  );
};

const EventListEvent = ({ event }) => {
  return (
    <li className="event-list event-list-item">
      <Link to={`/events/${event.id}`} className="event-list-link">
        <EventName name={event.name} lang="fi" styling="event-list-name" />
        <div className="event-list-dates event-list-small-text">
          <EventDate date={event.event_dates.starting_day} />
          <p> - </p>
          <EventDate date={event.event_dates.ending_day} />
        </div>
        <EventAddress
          address={event.location.address}
          postalCode={true}
          city={true}
          styling="event-list-dates event-list-small-text"
        />
      </Link>
    </li>
  );
};

export default EventListEvent;