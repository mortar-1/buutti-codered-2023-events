import { useContext } from "react";
import { useNavigate, Link } from "react-router-dom";

import { AppContext } from "../Context/AppContext";
import "./Navigation.css";

const BeforeLogin = () => (
  <>
    <Link to="/login" className="nav-link">Sign in</Link>
    <Link to="/register" className="nav-link">Sign up</Link>
  </>
);

const AfterLogin = ({ logout }) => (
  <>
    <Link to="/user/favourites" className="nav-link">Suosikit</Link>
    <a onClick={() => logout()} className="nav-link">Sign out</a>
  </>
);

const Navigation = () => {
  const { user, logoutUser } = useContext(AppContext);
  const navigate = useNavigate();

  const handleLogout = () => {
    logoutUser();
    navigate("/");
  };

  const NavLinks = user
    ? <AfterLogin logout={handleLogout} />
    : <BeforeLogin />;

  return (
    <nav>
      <Link to="/events" className="nav-link">Tapahtumat</Link>
      <Link to="/map" className="nav-link">Kartta</Link>
      {NavLinks}
    </nav>
  );
};

export default Navigation;