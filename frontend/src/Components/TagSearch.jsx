import { useState, useContext } from "react";

import { AppContext } from "../Context/AppContext";
import "./TagSearch.css";

const SelectedTagsList = ({ selectedTags, clearTags }) => {
  if (!selectedTags.length) return null;

  return (
    <>
      {selectedTags.map((t, i) =>
        <p key={i} className="selected-tag">{t}</p>
      )}
      <button onClick={() => clearTags()}>Tyhjennä</button>
    </>
  );
};

const TagPicker = ({ searchValue, tags, addTag }) => {
  if (!tags) return null;
  let filteredTags = [];
  if (searchValue.length > 2) {
    filteredTags = tags.filter(t =>
      t.toLowerCase().startsWith(searchValue.toLowerCase())
    ).slice(0, 10);
  }

  if (!filteredTags.length) return null;

  return (
    <div className="tag-picker-wrapper">
      {filteredTags.map((t, i) =>
        <button
          key={i}
          onClick={() => addTag(t)}
          className="tag-pick-button"
        >{t}</button>
      )}
    </div>
  );
};

const TagSearch = ({ apiFetch, addTag, selectedTags, clearTags }) => {
  const { tags } = useContext(AppContext);
  const [input, setInput] = useState("");

  const handleSubmit = async event => {
    event.preventDefault();
    setInput("");
    await apiFetch();
  };

  const addSelectedTag = tag => {
    addTag(tag);
    setInput("");
  };

  return (
    <section className="tag-search-wrapper">
      <h3>Hae tageilla:</h3>
      <form onSubmit={handleSubmit} className="tag-form">
        <input
          className="tag-search-input"
          value={input}
          onChange={e => setInput(e.target.value)}
        />
        <TagPicker
          searchValue={input}
          tags={tags}
          addTag={addSelectedTag}
        />
        <SelectedTagsList
          selectedTags={selectedTags}
          clearTags={clearTags}
        />
        <button>Hae</button>
      </form>
    </section>
  );
};

export default TagSearch;