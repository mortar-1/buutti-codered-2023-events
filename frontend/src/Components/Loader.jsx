const Loader = ({ isLoading }) => {
  if (!isLoading) {
    return null;
  }

  return (
    <div>
      <p>Heleventa på litet...</p>
    </div>
  );
};

export default Loader;