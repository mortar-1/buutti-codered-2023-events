import EventListEvent from "./EventListEvent";
import "./EventList.css";

const EventList = ({ events }) => {
  if (!events || !events.length) {
    return null;
  }

  return (
    <div>
      <ul className="event-list">
        {events.map((e, i) =>
          <EventListEvent
            key={e.id + i}
            event={e}
          />
        )}
      </ul>
    </div>
  );
};

export default EventList;