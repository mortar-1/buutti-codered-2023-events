FROM node:16-alpine

WORKDIR /usr/src/app

ARG PG_HOST
ARG PG_USERNAME
ARG PG_DATABASE
ARG PG_PASSWORD
ARG PG_PORT

ENV PG_HOST=${PG_HOST}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_DATABASE=${PG_DATABASE}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_PORT=${PG_PORT}
ENV PORT 3000
ENV NODE_ENV production

COPY /backend/package*.json ./
COPY /backend/index.js ./index.js
COPY /backend/db/ ./db/
COPY /backend/dist/ ./dist/

RUN npm install

EXPOSE 3000

CMD ["npm", "run", "start"]