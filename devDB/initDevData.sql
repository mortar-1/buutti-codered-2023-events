CREATE TABLE IF NOT EXISTS users (
  id SERIAL,
  username varchar(20) NOT NULL UNIQUE,
  password varchar(20) NOT NULL,
  PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS favorites (
  id SERIAL,
  event_id varchar NOT NULL,
  name varchar NOT NULL,
  end_date varchar,
  user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
  PRIMARY KEY(id)
);

INSERT INTO
  users (username, password)
VALUES
  ('user', 'user'),
  ('admin', 'admin')
;

INSERT INTO
  favorites (event_id, name, end_date, user_id)
VALUES
  ('helsinki:agcxcycqbi', 'Some nice event', '2023-04-23T19:00:00.000Z', 1),
  ('helsinki:agcxcycnuu', 'Billy bonkas weird fun', '2023-05-08T19:00:00.000Z', 1),
  ('helmet:243408', 'Pokemon go party event', '2023-02-14T19:00:00.000Z', 1),
  ('helmet:250741', 'JSC work practice', '2023-01-10T19:00:00.000Z', 1),
  ('helsinki:afy2nwdv4u', 'JSC work practice', null, 1)
;