# GROUP RED project 2

Events myhelsinki-api full stack project

## Dev
### Commands
Scripts in root package.json

Run development database:
  - npm run dev:up (starts docker postgres db, CTRL + C to close)
  - npm run dev:build (starts and builds postgres db)
  - npm run dev:down (removes docker container)

Lint:
  - npm run lint (runs eslint)
  - npm run lint:fix (runs eslint and fixes the style errors)

### Git
Read git-workflow.md file and follow instructions